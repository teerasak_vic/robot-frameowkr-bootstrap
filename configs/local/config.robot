*** Variables ***

# DB CONFIG
${DB_HOSTNAME}          alpha-scmdb-rw.wemall-dev.com
${DB_USERNAME}          root
${DB_PASSWORD}          Welcome1
${DB_NAME}              scm_db
${DB_CHARSET}           utf8
${DB_PORT}              3306

# API END POINT
${SCM_API_DIRECT_HOSTNAME}  alpha-scm-api.wemall-dev.com
${SCM_API_HOSTNAME}     alpha-api.wemall-dev.com
${FMS_API_HOSTNAME}     alpha-api.aden-dev.asia

# UI END POINT
${SCM_UI_HOSTNAME}      https://alpha-scm.wemall-dev.com

# API KEY
${SCM_API_KEY}          5666a0d8696e9d33aa000001277e2e2857394df76eb3378c1301d52b
${FMS_API_KEY}          577f5939287cf028a20000018e68e1d5948c4c027fee33a156a97e87
