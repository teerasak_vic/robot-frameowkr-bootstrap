## Initialize script to use in every robot framework file

*** Settings ***
Resource    ./../configs/default.robot
Resource    ./../configs/${ENV}/config.robot
Resource    ./../configs/endpoints.robot

Library     ./../libraries/init.py
