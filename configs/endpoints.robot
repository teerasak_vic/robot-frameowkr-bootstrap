*** Variables ***
#  and 
# read from ./configs/{{env}}/config.robot

# API END POINT
${API_PO}           /scm/api/v1/po
${API_STOCK}        /scm/api/v1/stock
${API_SKU}          /scm/api/v1/sku
${API_PO_APPEND}    /scm/api/v1/po/append
${API_GR}           /scm/api/v1/gr
${FMS_GET_STOCK}    /fms-api/scm/{{CLIENT}}/stock

# UI END POINT
${UI_PO_LIST}         /po
${UI_PO_DETAIL}       /po/
${UI_SKU_LIST}        /sku
${UI_DASHBOARD_MAIN}  /bu-select
${UI_DASHBOARD_BU}    /bu/
