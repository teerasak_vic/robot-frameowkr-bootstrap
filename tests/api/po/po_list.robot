*** Settings ***
Resource    ./../../../configs/init.robot

Resource    ./../../../keywords/api.robot
Resource    ./../../../keywords/tools.robot

*** Test Cases ***
TC_SCM_00170 setup / teardown via sql script
    [Tags]      TC_SCM_00170  ITMA-3344  SCM-Sprint 2016S3  success  ready
    [Setup]     execute_sql_script  ./fixtures/TC_SCM_00170-setup.sql
    [Teardown]  execute_sql_script  ./fixtures/TC_SCM_00170-teardown.sql

    API Health Should OK
    # ${response}=    Send Http Get Request on SCM API    ${API_PO}?po_number=TC_SCM_00170_PO001&bu_code=BTH003
    # Response Header Status Should be SUCCESS

    # Expected Size of Item in Current Page Should be Equal  ${response}  /data/item  1

TC_SCM_00171 setup / teardown via csv
    [Tags]      TC_SCM_00170  ITMA-3344  SCM-Sprint 2016S3  success  ready
    [Setup]     insert_data_from_csv  sku  ./fixtures/TC_SCM_00171-setup.csv
    [Teardown]  delete_data_from_csv  sku  ./fixtures/TC_SCM_00171-teardown.csv

    API Health Should OK


*** Keywords ***
Expected Size of Item in Current Page Should be Equal
    [Arguments]  ${json}  ${actual_node}  ${expected_size}
    ${item} =  Get Json Value  ${json}  ${actual_node}
    ${item_json} =  Parse Json  ${item}
    Length Should Be  ${item_json}  ${expected_size}


#	Given API SKU - Create SKU  ${TC_SCM_00170_SKU1}  ${True}
#	and API PO - Create PO With Single PO Line  ${TC_SCM_00170_PO1}  ${True}
#
#	When API PO - Get PO List   po_number=${TC_SCM_00170_PO1.po_no}&bu_code=BTH003  2  ${True}
#	${actual_found_body}=  Get Response Body
#	√ Then API PO - Assert Total PO  ${actual_found_body}  1
#	${expect_data}=   Create List  ${TC_SCM_00170_PO1}
#	and API PO - Assert PO List Data   ${actual_found_body}  ${expect_data}
#
#	When API PO - Get PO List  po_number=TCM_SCM_00170_PO002&bu_code=BTH003  2  ${True}
#	${actual_not_found_body}=  Get Response Body
#	and API PO - Assert Do Not Have Po Data   ${actual_not_found_body}
#
#
#
#	[Teardown]    Run Keywords   Log To console   Teardown is working ...
#	...   AND    Delete SKU   ${TC_SCM_00170_SKU1.sku}
#	...   AND    Delete PO    ${TC_SCM_00170_PO1.po_no}

#TEST EXMAPLE
#    Log to Console  Yikes
#
#TEST TABLE DESCRIBE
#    ${xxx} =  Describe  sku
#    Log To Console  ${xxx["sku"]}
#
#TEST QUERY FROM FILE
#    ${result} =  Query  SELECT * FROM sku LIMIT 0, 1
#    Log to Console  ${result}
#
#TEST insert_data_from_csv
#    Insert Data From Csv   sku  fixtures/sku.csv
#
#TEST delete_data_from_csv
#    Delete Data From Csv   sku  fixtures/sku.csv
#
#TEST variable from config
#    Log to console  ${API_PO}
#
#TEST API HEALTH CHECK
#    API Health Should OK
