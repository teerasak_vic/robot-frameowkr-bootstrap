*** Settings ***
Library     ./../libraries/database_library.py
Library     ./../libraries/csv_library.py

Library     HttpLibrary.HTTP
Library     Collections

*** Keywords ***
Expected Json Equal
    [Arguments]  ${json}  ${actual_node}  ${expected_value}
    ${actual_value} =  Get Json Value  ${json}  ${actual_node}
    Should Be Equal  ${expected_value}  ${actual_value}
