*** Settings ***
Library  HttpLibrary.HTTP
Library  Collections
Library  String

*** Keywords ***

API Health Should OK
    ${response} =  Send Http Get Request  ${SCM_API_DIRECT_HOSTNAME}  /health
    Json Value Should Equal	 ${response}  /status  "UP"

Response Header Status Should be SUCCESS
    ${status}=      Get Response Status
    Should Start With  ${status}  2

Send Http Get Request on SCM API
    [Arguments]  ${path}
    ${response} =  Send Http Get Request  ${SCM_API_HOSTNAME}  ${path}
    [Return]  ${response}

Send Http Get Request
    [Arguments]  ${url}  ${path}
    Log to Console  URL = http://${url}${path}
    Create API Http Context  ${url}
    ${response} =   Perform GET Request  ${path}
    [Return]  ${response}

Create API Http Context
    [Arguments]  ${root}=${SCM_API_HOSTNAME}
    Create API Context     ${root}     http

Create API Https Context
    [Arguments]  ${root}=${SCM_API_HOSTNAME}
    Create API Context     ${root}     https

Create API Context
    [Arguments]  ${root}=${SCM_API_HOSTNAME}  ${http}=http
    Create Http Context   ${root}     ${http}
    Set Request Header    Content-Type    application/json
    Set Request Header    Authorization   ${SCM_API_KEY}

Perform GET Request
    [Arguments]  ${path}
    GET    ${path}
    ${response} =   Perform Request
    [Return]  ${response}

Perform POST Request
    [Arguments]  ${path}
    POST    ${path}
    ${response} =   Perform Request
    [Return]  ${response}

Perform Request
    Response Status Code Should Equal    200 OK
    ${raw_response}=    Get Response Body
    [Return]    ${raw_response}
