
-- &{TC_SCM_00170_SKU1}  mat_code=MATCODE0001
--  ...  sku=TC_SCM_00170_SKU001
--  ...  mat_code_type=sap_mat_code
--  ...  item_name=ITEM0001
--  ...  bu_code=BTH003
--  ...  bu_name=BTH003NAME
--  ...  source_type=MF
--  ...  uom_code=ea
--  ...  uom_conversion=1
--  ...  created_at=1471335139
--  ...  created_by=Antman Robot
INSERT INTO `sku` (
  `id`, `sku`, `mat_code`, `mat_code_type`, `item_name`, `bu_code`, `uom_code`, `uom_conversion`, `source_type`, `created_at`, `created_by`, `updated_at`, `updated_by`, `created_by_name`, `updated_by_name`)
VALUES
	(88881, 'TC_SCM_00170_SKU001', 'MATCODE0001', 'sap_mat_code', 'ITEM0001', 'BTH003', 'ea', 1, 'MF', '2016-09-28 10:21:16', 0, NULL, 0, 'Antman Robot', 'Antman Robot');

-- &{TC_SCM_00170_PO1}  po_no=TC_SCM_00170_PO001
--  ...   bu_code=BTH003
--  ...   supplier_name=TC_SCM_00170_SUP
--  ...   supplier_code=TC_SCM_00170_VENDOR_NUMBER_001
--  ...   pr_name=Robot Purchase Order Name
--  ...   due_date=1471922206
--  ...   status=open
--  ...   created_at=1471922206
--  ...   created_by=robot automated
--  ...   po_line1_sku=${TC_SCM_00170_SKU1.sku}
--  ...   po_line1_cost=100
--  ...   po_line1_qty=10
--  ...   po_line1_status=open
INSERT INTO `purchase_orders`
  (`id`, `po_no`, `client_id`, `bu_code`, `supplier_name`, `supplier_code`, `pr_name`, `due_date`, `status`, `total_price`, `created_at`, `created_by`, `updated_at`, `updated_by`, `created_by_name`, `updated_by_name`)
VALUES
	(88881, 'TC_SCM_00170_PO001', 2, 'BTH003', 'TC_SCM_00170_SUP', 'TC_SCM_00170_VENDOR_NUMBER_001', 'Robot Purchase Order Name', '2015-08-24 00:00:00', 'open', 0, '2016-09-28 10:21:20', 0, NULL, 0, 'robot automated', 'robot automated');

INSERT INTO `purchase_order_items`
  (`id`, `po_id`, `sku`, `mat_code`, `item_name`, `cost`, `qty`, `receive_qty`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`, `created_by_name`, `updated_by_name`)
VALUES
	(88881, 88881, 'TC_SCM_00170_SKU001', 'MATCODE0001', 'ITEM0001', 100, 10, 0, 'open', '2016-09-28 10:21:21', 0, NULL, 0, 'robot automated', 'robot automated');
