# FILES: csv.py

# IMPORT: Robot Framework Library
from robot.api import logger

# IMPORT: Python Library
import csv
from collections import deque

# IMPORT: Custom Library
from database_library import execute_sql_string

# DEF:
def read_csv_file(filename):
    """Read CSV file and return its content as a Python list.
    Use Robot Framework's Collections library to futher manipulate lists.
    """

    f = open(filename, 'r')
    data = csv.reader(f)
    f.close
    return deque([row for row in data])


def insert_data_from_csv(table, filename):
    data = read_csv_file(filename)
    insertRecord = []
    insertColumn = data.popleft()

    for v in data:
        insertRecord.append("('{0}')".format("','".join(v)).replace("''", "NULL"))

    insertStatement = "INSERT INTO {0} ({1}) VALUES {2}".format(table, ",".join(insertColumn), ",".join(insertRecord))
    logger.console(insertStatement)
    execute_sql_string(insertStatement)


def delete_data_from_csv(table, filename):
    data = read_csv_file(filename)
    removeId = []
    data.popleft()

    for v in data:
        removeId.append(v[0])

    deleteStatement = "DELETE FROM {0} WHERE id IN ({1})".format(table, ",".join(removeId))
    logger.console(deleteStatement)
    execute_sql_string(deleteStatement)

