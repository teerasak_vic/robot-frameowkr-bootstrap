# FILES: init.py

# IMPORT: Robot Framework Library
from robot.libraries.BuiltIn import BuiltIn
from robot.api import logger

# IMPORT: Python Library

# DEF: 
def printEnv():
    logger.console("> Robot Framework Start on ENV = {0}".format(ENV))
    logger.console("> have a nice day :>\n")

# INT: Read Config
try:
    ENV         = BuiltIn().get_variable_value("${ENV}")
    DB_HOSTNAME = BuiltIn().get_variable_value("${DB_HOSTNAME}")
    DB_USERNAME = BuiltIn().get_variable_value("${DB_USERNAME}")
    DB_PASSWORD = BuiltIn().get_variable_value("${DB_PASSWORD}")
    DB_NAME     = BuiltIn().get_variable_value("${DB_NAME}")
    DB_CHARSET  = BuiltIn().get_variable_value("${DB_CHARSET}")

    printEnv()

except Exception as err:
    logger.console("ERROR: {0}".format(err))
    raise
