# FILES: database.py

# IMPORT: Robot Framework Library
from robot.api import logger

# IMPORT: Python Library
import MySQLdb as mysql

# IMPORT: Custom Library
from init import *

# DEF:
def describe(tableName):
    """
    Display table fields data
    :param tableName:
    :return: collection of (field, type)
    """
    result = {}
    for row in query("DESCRIBE {0}".format(tableName)):
        result.setdefault(row.get('Field'), row.get('Type'))
    return result


def query(sqlStatement):
    """
    Execute SQL Query
    :param sqlStatement:
    :return: queryResult
    """
    DB.execute(sqlStatement)
    return DB.fetchall()


def row_count(sqlStatement):
    """
    Get result row count
    :param sqlStatement:
    :return:
    """
    query(sqlStatement)
    return DB.rowcount


def execute_sql_script(sqlScriptFileName):
    """
    Executes the content of the `sqlScriptFileName` as SQL commands.
    Useful for setting the database to a known state before running
    your tests, or clearing out your test data after running each a
    test.

    Sample usage :
    | Execute Sql Script | ${EXECDIR}${/}resources${/}DDL-setup.sql |
    | Execute Sql Script | ${EXECDIR}${/}resources${/}DML-setup.sql |
    | #interesting stuff here |
    | Execute Sql Script | ${EXECDIR}${/}resources${/}DML-teardown.sql |
    | Execute Sql Script | ${EXECDIR}${/}resources${/}DDL-teardown.sql |

    SQL commands are expected to be delimited by a semi-colon (';').

    For example:
    DELETE FROM person_employee_table;
    DELETE FROM person_table;
    DELETE FROM employee_table;

    Also, the last SQL command can optionally omit its trailing semi-colon.

    For example:
    DELETE FROM person_employee_table;
    DELETE FROM person_table;
    DELETE FROM employee_table

    Given this, that means you can create spread your SQL commands in several
    lines.

    For example:
    DELETE
      FROM person_employee_table;
    DELETE
      FROM person_table;
    DELETE
      FROM employee_table

    However, lines that starts with a number sign (`#`) are treated as a
    commented line. Thus, none of the contents of that line will be executed.

    For example:
    # Delete the bridging table first...
    DELETE
      FROM person_employee_table;
      # ...and then the bridged tables.
    DELETE
      FROM person_table;
    DELETE
      FROM employee_table
    """
    sqlScriptFile = open(sqlScriptFileName)
    try:
        sqlStatement = ''
        for line in sqlScriptFile:
            line = line.strip()
            if line.startswith('#'):
                continue
            elif line.startswith('--'):
                continue

            sqlFragments = line.split(';')
            if len(sqlFragments) == 1:
                sqlStatement += line + ' '
            else:
                for sqlFragment in sqlFragments:
                    sqlFragment = sqlFragment.strip()
                    if len(sqlFragment) == 0:
                        continue

                    sqlStatement += sqlFragment + ' '

                    DB.execute(sqlStatement)
                    sqlStatement = ''

        sqlStatement = sqlStatement.strip()
        if len(sqlStatement) != 0:
            DB.execute(sqlStatement)

        DB_CONNECTION.commit()
    finally:
        DB_CONNECTION.rollback()


def execute_sql_string(sqlString):
    """
    Executes the sqlString as SQL commands.
    Useful to pass arguments to your sql.

    SQL commands are expected to be delimited by a semi-colon (';').

    For example:
    | Execute Sql String | DELETE FROM person_employee_table; DELETE FROM person_table |

    For example with an argument:
    | Execute Sql String | SELECT * FROM person WHERE first_name = ${FIRSTNAME} |
    """
    try:
        DB.execute(sqlString)
        DB_CONNECTION.commit()
    finally:
        DB_CONNECTION.rollback()


def check_if_exists_in_database(selectStatement):
    """
    Check if any row would be returned by given the input
    `selectStatement`. If there are no results, then this will
    throw an AssertionError.

    For example, given we have a table `person` with the following data:
    | id | first_name  | last_name |
    |  1 | Franz Allan | See       |

    When you have the following assertions in your robot
    | Check If Exists In Database | SELECT id FROM person WHERE first_name = 'Franz Allan' |
    | Check If Exists In Database | SELECT id FROM person WHERE first_name = 'John' |

    Then you will get the following:
    | Check If Exists In Database | SELECT id FROM person WHERE first_name = 'Franz Allan' | # PASS |
    | Check If Exists In Database | SELECT id FROM person WHERE first_name = 'John' | # FAIL |
    """
    if not row_count(selectStatement):
        raise AssertionError("Expected to have have at least one row from '%s' "
                             "but got 0 rows." % selectStatement)


def check_if_not_exists_in_database(selectStatement):
    """
    This is the negation of `check_if_exists_in_database`.

    Check if no rows would be returned by given the input
    `selectStatement`. If there are any results, then this will
    throw an AssertionError.

    For example, given we have a table `person` with the following data:
    | id | first_name  | last_name |
    |  1 | Franz Allan | See       |

    When you have the following assertions in your robot
    | Check If Not Exists In Database | SELECT id FROM person WHERE first_name = 'John' |
    | Check If Not Exists In Database | SELECT id FROM person WHERE first_name = 'Franz Allan' |

    Then you will get the following:
    | Check If Not Exists In Database | SELECT id FROM person WHERE first_name = 'John' | # PASS |
    | Check If Not Exists In Database | SELECT id FROM person WHERE first_name = 'Franz Allan' | # FAIL |
    """
    queryResults = query(selectStatement)
    if queryResults:
        raise AssertionError("Expected to have have no rows from '%s' "
                             "but got some rows : %s." % (selectStatement, queryResults))


def row_count_is_0(selectStatement):
    """
    Check if any rows are returned from the submitted `selectStatement`.
    If there are, then this will throw an AssertionError.

    For example, given we have a table `person` with the following data:
    | id | first_name  | last_name |
    |  1 | Franz Allan | See       |

    When you have the following assertions in your robot
    | Row Count is 0 | SELECT id FROM person WHERE first_name = 'Franz Allan' |
    | Row Count is 0 | SELECT id FROM person WHERE first_name = 'John' |

    Then you will get the following:
    | Row Count is 0 | SELECT id FROM person WHERE first_name = 'Franz Allan' | # FAIL |
    | Row Count is 0 | SELECT id FROM person WHERE first_name = 'John' | # PASS |
    """
    num_rows = row_count(selectStatement)
    if num_rows > 0:
        raise AssertionError("Expected zero rows to be returned from '%s' "
                             "but got rows back. Number of rows returned was %s" % (selectStatement, num_rows))


def row_count_is_equal_to_x(selectStatement,numRows):
    """
    Check if the number of rows returned from `selectStatement` is equal to
    the value submitted. If not, then this will throw an AssertionError.

    For example, given we have a table `person` with the following data:
    | id | first_name  | last_name |
    |  1 | Franz Allan | See       |
    |  2 | Jerry       | Schneider |

    When you have the following assertions in your robot
    | Row Count Is Equal To X | SELECT id FROM person | 1 |
    | Row Count Is Equal To X | SELECT id FROM person WHERE first_name = 'John' | 0 |

    Then you will get the following:
    | Row Count Is Equal To X | SELECT id FROM person | 1 | # FAIL |
    | Row Count Is Equal To X | SELECT id FROM person WHERE first_name = 'John' | 0 | # PASS |
    """
    num_rows = row_count(selectStatement)
    if num_rows != int(numRows.encode('ascii')):
        raise AssertionError("Expected same number of rows to be returned from '%s' "
                             "than the returned rows of %s" % (selectStatement, num_rows))


def row_count_is_greater_than_x(selectStatement,numRows):
    """
    Check if the number of rows returned from `selectStatement` is greater
    than the value submitted. If not, then this will throw an AssertionError.

    For example, given we have a table `person` with the following data:
    | id | first_name  | last_name |
    |  1 | Franz Allan | See       |
    |  2 | Jerry       | Schneider |

    When you have the following assertions in your robot
    | Row Count Is Greater Than X | SELECT id FROM person | 1 |
    | Row Count Is Greater Than X | SELECT id FROM person WHERE first_name = 'John' | 0 |

    Then you will get the following:
    | Row Count Is Greater Than X | SELECT id FROM person | 1 | # PASS |
    | Row Count Is Greater Than X | SELECT id FROM person WHERE first_name = 'John' | 0 | # FAIL |
    """
    num_rows = row_count(selectStatement)
    if num_rows <= int(numRows.encode('ascii')):
        raise AssertionError("Expected more rows to be returned from '%s' "
                             "than the returned rows of %s" % (selectStatement, num_rows))


def row_count_is_less_than_x(selectStatement,numRows):
    """
    Check if the number of rows returned from `selectStatement` is less
    than the value submitted. If not, then this will throw an AssertionError.

    For example, given we have a table `person` with the following data:
    | id | first_name  | last_name |
    |  1 | Franz Allan | See       |
    |  2 | Jerry       | Schneider |

    When you have the following assertions in your robot
    | Row Count Is Less Than X | SELECT id FROM person | 3 |
    | Row Count Is Less Than X | SELECT id FROM person WHERE first_name = 'John' | 1 |

    Then you will get the following:
    | Row Count Is Less Than X | SELECT id FROM person | 3 | # PASS |
    | Row Count Is Less Than X | SELECT id FROM person WHERE first_name = 'John' | 1 | # FAIL |
    """
    num_rows = row_count(selectStatement)
    if num_rows >= int(numRows.encode('ascii')):
        raise AssertionError("Expected less rows to be returned from '%s' "
                             "than the returned rows of %s" % (selectStatement, num_rows))


# INT: Create DB connection
try:
    logger.console("> Connect DB")
    logger.console("> with config {1}@{0}/{2}\n".format(DB_HOSTNAME, DB_USERNAME, DB_NAME))

    DB_CONNECTION = mysql.connect(host=DB_HOSTNAME, user=DB_USERNAME, passwd=DB_PASSWORD, db=DB_NAME, charset=DB_CHARSET)
    DB = DB_CONNECTION.cursor(mysql.cursors.DictCursor)

except Exception as err:
    logger.console("ERROR: {0}".format(err))
    raise
